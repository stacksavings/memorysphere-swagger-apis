'use strict';
var util = require('util');

module.exports = {
  getAvailableFeeds: getAvailableFeeds,
  saveFeedsChoosen: saveFeedsChoosen,
};

function getAvailableFeeds(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
    res.send(["Sports"]);
  // this sends back a JSON response which is a single string
}

function saveFeedsChoosen(req, res){
	res.send({"status": "success"});
}