'use strict';
var util = require('util');

module.exports = {
  getUserProfile: getUserProfile,
  saveUserProfile: saveUserProfile,
  verifyChangedEmail: verifyChangedEmail,
  verifyChangedMobile: verifyChangedMobile
};

function getUserProfile(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
    res.send({"firstname": "", "lastname": "", "dob":"" , "email": ""});
  // this sends back a JSON response which is a single string
}

function saveUserProfile(req, res){
	res.send({"status": "success"});
}

function verifyChangedEmail(req, res){
	res.send({"status": "success", "message": "Email sent to specified email id. Please click on verification link to verify email."});
}

function verifyChangedMobile(req, res){
	res.send({"status": "success", "message": "Sms sent to specified mobile. Please enter code receiverd in sms to verify mobile."});
}